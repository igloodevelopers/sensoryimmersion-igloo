﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalFearTrainingUI : MonoBehaviour
{
    private GameObject selectedAnimal;
    private string selectedAnimalName;
    private Material selectedMaterialVarient;

    private Transform selectedDistance;
    private Behaviour behaviour = Behaviour.passive;

    private List<string> materialVarients = new List<string>();
    private List<string> animalVarients = new List<string>();

    public Button BeginSimulationButton;
    public Transform spawnLocation; 

    public Dropdown behaviourDropdown;
    [System.Serializable]
    public enum Behaviour
    {
        passive, excited, aggressive
    }

    public Waypoints waypoints;
    [System.Serializable]
    public struct Waypoints
    {
        public Transform far;
        public Transform middle;
        public Transform near; 
    }

    public Dropdown animalDropdown;
    public Dropdown skinDropdown;
    public Animals[] animals;
    [System.Serializable]
    public struct Animals
    {
        public string AnimalName;
        public Material[] skinVarients;
        public GameObject AnimalPrefab;
    }

    public Text selectedAnimalDistanceLabel;

    public Transform waypoint1, waypoint2, waypoint3, waypoint4; 

    public void Start()
    {
        // Clear all dropdowns before we start.
        animalDropdown.ClearOptions();
        skinDropdown.ClearOptions();
        behaviourDropdown.ClearOptions();
        BeginSimulationButton.interactable = false;
        skinDropdown.interactable = false;
        PopulateAnimalDropdown();
        PopulateBehaviourDropdown();
        selectedAnimalDistanceLabel.text = "Far";
    }

    private void PopulateAnimalDropdown()
    {
        foreach (Animals animal in animals)
        {
            animalVarients.Add(animal.AnimalName);
        }
        animalDropdown.AddOptions(animalVarients);
        animalDropdown.onValueChanged.AddListener(delegate { PopulateSkinDropdown(animalDropdown); });
        PopulateSkinDropdown(animalDropdown);
    }

    public void PopulateBehaviourDropdown()
    {
        behaviourDropdown.AddOptions(new List<string>() { "Passive", "Aggressive", "Excited" });
        behaviourDropdown.onValueChanged.AddListener(delegate { SelectBehaviourVarient(behaviourDropdown); });
    }

    public void PopulateSkinDropdown(Dropdown animalSelection)
    {
        foreach (Animals animal in animals)
        {
            if(animal.AnimalName == animalSelection.options[animalSelection.value].text)
            {
                // Select the animal
                selectedAnimal = animal.AnimalPrefab;
                // select the first skin as defaul
                selectedMaterialVarient = animal.skinVarients[0];
                // make the begin button interactable
                BeginSimulationButton.interactable = true;
                // Set all the labels to the selected presets

                // Clear the list
                materialVarients.Clear();
                // Populate the list with new animal skins
                foreach (Material mat in animal.skinVarients)
                {
                    materialVarients.Add(mat.name);
                }
                // Set everything up in the dropdown
                skinDropdown.interactable = true;
                skinDropdown.AddOptions(materialVarients);
                skinDropdown.onValueChanged.AddListener(delegate { SelectMaterialVarient(skinDropdown); });
            }
        }
    }

    public void SelectMaterialVarient(Dropdown varient)
    {
        foreach (Animals animal in animals)
        {
            if (animal.AnimalPrefab == selectedAnimal)
            {
                foreach (Material mat in animal.skinVarients)
                {
                    if(mat.name == skinDropdown.options[varient.value].text)
                    {
                        selectedMaterialVarient = mat;
                        return;
                    }
                }
            }
        }
    }

    public void SelectBehaviourVarient(Dropdown behaviour)
    {
        switch (behaviourDropdown.options[behaviour.value].text)
        {
            case "Passive":

                break;
            case "Aggressive":

                break;
            case "Excited":

                break;
        }
    }

    public void BeginSimulation()
    {
        GameObject animalPrefab = Instantiate(selectedAnimal) as GameObject;
        animalPrefab.transform.position = spawnLocation.position;
        animalPrefab.GetComponentInChildren<Renderer>().material = selectedMaterialVarient;
        animalPrefab.GetComponent<Patrol>().points = new Transform[] { waypoint1, selectedDistance, waypoint2, waypoint3, waypoint4 }; 
    }

    public void AdjustAnimalDistanceToPlayer(float value)
    {
        value = (int)value;
        switch (value)
        {
            case 0:
                selectedDistance = waypoints.far;
                selectedAnimalDistanceLabel.text = "Far";
                break;
            case 1:
                selectedDistance = waypoints.middle;
                selectedAnimalDistanceLabel.text = "Middle";
                break;
            case 2:
                selectedDistance = waypoints.near;
                selectedAnimalDistanceLabel.text = "Near";
                break;
        }
    }
}
